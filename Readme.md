				    
                                                        	   ÁRBOLES BALANCEADOS
							        ------------------------
+ Empezando
    El programa cosiste en la creación de un árbol solo con números enteros, inicialmente se presenta un menú con 4 opciones. La primera opción es agregar números al árbol. La segunda opción es eliminar un nodo, en esta se le preguntara cual es el valor que quiere eliminar y luego se eliminara. La tercera opción es modificar valores primero se preguntara el valor del nodo que se quiere modificar y después el nuevo valor que este tendra. La cuarta opción le permite observar el contenido del árbol de forma gráfica y por último para salir debe presionar 5.

+ Requisitos previos  
   Graphviz.
   Para instalar Graphviz debe usar los comandos: 
	[1] sudo apt-get install graphviz
	[2] sudo apt-get update

+ Ejecutando las pruebas por terminal
    Para la entrada a los archivos en el editor de texto vim, se necesita del comando: 
        [1] vim nombrearchivo.h para archivos .h donde se encuemtra la definición de la clase.
	[2] vim nombrearchivo.cpp para archivos .cpp donde esta la implementación de la clase.
	
    Para compilar el programa se utiliza el comando:
	[3] make

    Mientras que para ejecutar debemos colocar:
        [4] ./programa
         

+ Construido con
    Sistema operativo: Ubuntu 18.04.3 LTS.
    Vim: Editor de texto para escribir el código del programa. 
    C++: Lenguaje de programación imperativo orientado a objetos derivado del lenguaje C.
    Pep-8: La narración del código realizado en este proyecto esta basado en las instrucciones dadas por la pep-8.
    graphviz: Paquete de visualización.

+ Versiones
    Ubuntu 18.04.3 LTS.
    c++ (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0

+ Autor
    Nicole Soto.


#include <iostream>
// Definición del nodo
#include "programa.h"

#ifndef ARBOL_H
#define ARBOL_H

class Arbol{
    private:

    public:
        Arbol();
        Nodo* crear_nodo(int numero, Nodo *padre);
        void insertar(Nodo *&raiz, int numero, bool &altura, Nodo *padre);
        void eliminar(Nodo *&raiz, bool &altura, int numero);

        void reestructuracion_izq(bool altura, Nodo *&raiz);
        // Rotaciones para la reestructuración izquierda
        void rotacion_DD(Nodo *&raiz, bool altura, Nodo *nodo1);
        void rotacion_DI(Nodo *&raiz, Nodo *nodo1, Nodo *nodo2);

        void reestructuracion_der(bool altura, Nodo *&raiz);
        // Rotaciones para la reestructuración derecha
        void rotacion_II(Nodo *&raiz, bool altura, Nodo *nodo1);
        void rotacion_ID(Nodo *&raiz, Nodo *nodo1, Nodo *nodo2);
};
#endif

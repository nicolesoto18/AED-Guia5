#include <iostream>
// Definición de la clase
#include "Arbol.h"

using namespace std;

// Constructor vacio
Arbol::Arbol(){

}

// Crea un nuevo nodo
Nodo* Arbol::crear_nodo(int numero, Nodo *padre){
    Nodo *temporal;

    temporal = new Nodo;  

    // Se le asigna un valor al nodo
    temporal->dato = numero;

    // Se crean los subarboles izquierdo y derecho
    temporal->izq = NULL;
    temporal->der = NULL;
    temporal->nodo_padre = padre;
    
    return temporal;
}
// Rotación de las ramas derechas
void Arbol::rotacion_DD(Nodo *&raiz, bool altura, Nodo *nodo1){
    raiz->der = nodo1->izq;
    nodo1->izq = raiz; 
    
    // Actualiza el factor de cambio de los nodos modificados 
    if(nodo1->factor_equilibrio == 0){
        raiz->factor_equilibrio = 1;
        nodo1->factor_equilibrio = -1;
        altura = false;
    }
    
    else if(nodo1->factor_equilibrio == 1){
        raiz->factor_equilibrio = 0;
        nodo1->factor_equilibrio = 0;
    }
    
    // Se asigna un nuevo valor a la raiz
    raiz = nodo1;
}

// Rotación primero de ramas derechas y luego izquierdas
void Arbol::rotacion_DI(Nodo *&raiz, Nodo *nodo1, Nodo *nodo2){
    nodo2 = nodo1->izq;
    raiz->der = nodo2->izq;
    nodo2->izq = raiz; 
    nodo1->izq = nodo2->der;
    nodo2->der = nodo1;

    if(nodo2->factor_equilibrio == 1){
        raiz->factor_equilibrio = -1;
    }

    else{
        raiz->factor_equilibrio = 0;
    }

    if(nodo2->factor_equilibrio == -1){
        nodo1->factor_equilibrio = 1;
    }

    else{
        nodo1->factor_equilibrio = 0;
    }
    
    raiz = nodo2;
    nodo2->factor_equilibrio = 0;
}

void Arbol::reestructuracion_izq(bool altura, Nodo *&raiz){
    Nodo *nodo1;
    Nodo *nodo2;

    if(altura == true){
        // Verifica el valor del factor de equilibrio para identificar si se realizara una reestructuración
        if(raiz->factor_equilibrio == -1){
            raiz->factor_equilibrio = 0;
        }

        else if(raiz->factor_equilibrio == 0){
            raiz->factor_equilibrio = 1;
            altura = false;
        }

        else if(raiz->factor_equilibrio == 1){
            nodo1 = raiz->der;
            // Identifica que tipo de rotación se debe realizar
            if(nodo1->factor_equilibrio >= 0){
               rotacion_DD(raiz, altura, nodo1);
            }

            else{
                rotacion_DI(raiz, nodo1, nodo2);
            }
        }
    }
}

// Rotación de ramas izquierdas
void Arbol::rotacion_II(Nodo *&raiz, bool altura, Nodo *nodo1){
    raiz->izq = nodo1->der;
    nodo1->der = raiz; 
    
    if(nodo1->factor_equilibrio == 0){
        raiz->factor_equilibrio = -1;
        nodo1->factor_equilibrio = 1;
        altura = false;
    }
    
    else if(nodo1->factor_equilibrio == -1){
        raiz->factor_equilibrio = 0;
        nodo1->factor_equilibrio = 0;
    }

    // Se asigna un nuevo valor a la raiz
    raiz = nodo1;
}

// Rotación primero de ramas izquierdas y luego derechas
void Arbol::rotacion_ID(Nodo *&raiz, Nodo *nodo1, Nodo *nodo2){
    nodo2 = nodo1->der;
    raiz->izq = nodo2->der;
    nodo2->der = raiz; 
    nodo1->der = nodo2->izq;
    nodo2->izq = nodo1;

    if(nodo2->factor_equilibrio == -1){
        raiz->factor_equilibrio = 1;
    }

    else{
        raiz->factor_equilibrio = 0;
    }

    if(nodo2->factor_equilibrio == 1){
        nodo1->factor_equilibrio = -1;
    }

    else{
        nodo1->factor_equilibrio = 0;
    }
    
    raiz = nodo2;
    nodo2->factor_equilibrio = 0;
}

void Arbol::reestructuracion_der(bool altura, Nodo *&raiz){
    Nodo *nodo1;
    Nodo *nodo2;
    

    if(altura == true){
        // Verifica el valor del factor de equilibrio para identificar si se realizara una reestructuración
        if(raiz->factor_equilibrio == 1){
            raiz->factor_equilibrio = 0;
        }

        else if(raiz->factor_equilibrio == 0){
            raiz->factor_equilibrio = -1;
            altura = false;
        }

        else if(raiz->factor_equilibrio == -1){
            nodo1 = raiz->izq;
            // Identifica que tipo de rotación se debe realizar
            if(nodo1->factor_equilibrio <= 0){
                rotacion_II(raiz, altura, nodo1);
            }

            else{
                rotacion_ID(raiz, nodo1, nodo2);
            }
        }
    }
}

// Insertara el nodo en el subarbol correspondiente
void Arbol::insertar(Nodo *&raiz, int numero, bool &altura, Nodo *padre){
    int valor_raiz;

    if(raiz == NULL){
        Nodo *temporal;
        temporal = crear_nodo(numero, padre);
        raiz = temporal;
        insertar(raiz, numero, altura, padre);
    }
    
    else{
        valor_raiz = raiz->dato;
        
        // Inserta nodos en el subárbol izquierdo 
        if(numero < valor_raiz){
            // Llamada recursiva para agregar nuevos nodos 
            insertar(raiz->izq, numero, altura, raiz);
            reestructuracion_der(altura, raiz);
            
        }
        
        // Inserta nodos en el subárbol derecho 
        else if(numero > valor_raiz){
            // Llamada recursiva para agregar nuevos nodos 
            insertar(raiz->der, numero, altura, raiz);
            reestructuracion_izq(altura, raiz);
        }
       
        // Muestra que el número ya se encuentra en el árbol 
        else{
            cout << numero << " se encuentra en el árbol";
        }
    }
    
    altura = true;
}

void Arbol::eliminar(Nodo *&raiz, bool &altura, int numero){
    Nodo *temporal;
    Nodo *aux1;
    Nodo *aux2;
    bool bandera;

    if(raiz != NULL){
        // Se identifica el subárbol al que pertenece el dato
        if(numero < raiz->dato){
            // Llamada recursiva para eliminar el número
            eliminar(raiz->izq, altura, numero);
            // Reestruturación de los elementos restantes
            reestructuracion_izq(altura, raiz);
        }
        
        else if(numero > raiz->dato){
            eliminar(raiz->der, altura, numero);
            reestructuracion_der(altura, raiz);
        }
        
        else{
            temporal = raiz;
            
            // Subárbol derecho vacio
            if(temporal->der == NULL){
                raiz = temporal->izq;
            }
            
            else{
                // Subárbol izquierdo vacio
                if(temporal->izq == NULL){
                    raiz = temporal->der;
                }
                
                else{ // Elementos con dos descendientes
                    aux1 = raiz->izq;
                    bandera = false;
                   
                    // Se sustituye por el nodo más a la derecha 
                    while(aux1->der != NULL){
                        aux2 = aux1;
                        aux1 = aux1->der;
                        bandera = true;
                    }

                    // Se cambia el dato para la nueva raiz
                    raiz->dato = aux1->dato;
                    temporal = aux1;
                    
                    if(bandera == true){
                        aux2->der = aux1->izq;
                    }
                    
                    else{
                        raiz->izq = aux1->izq;
                        free(temporal); // Liberar memoria
                    }
                }
            }
        }
    }
    
    else{
        cout << "La información no se encuentra en el árbol";
    }
}

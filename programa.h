typedef struct _Nodo {
    int dato;
    int factor_equilibrio; // altura del subarbol derecho menos la altura del subarbol izquierdo

    struct _Nodo *izq;
    struct _Nodo *der;
    struct _Nodo *nodo_padre;
} Nodo;

